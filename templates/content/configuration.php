<div id="contentTable">
    <center><h2 >Configuration</h2></center>
	<table id="configuration" class="display" style="table-layout: fixed; font-size:11px; width: 100%; white-space: pre-wrap;">
        <thead>
            <tr>
                <th>Entreprise</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Siret</th>
                <th>Siren</th>
                <th>Telephone</th>
                <th>Mail</th>
                <th>Adresse</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
	</table>
    <div class="bootstrap-iso">
        <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 5">
            <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <strong class="me-auto">Enregistrement</strong>
                </div>
                <div class="toast-body">
                    Les modifications ont été enregistrées
                </div>
            </div>
        </div>
    </div>
</div>