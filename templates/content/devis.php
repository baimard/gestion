<div id="contentTable">
    <div class="breadcrumb" data-html2canvas-ignore>
        <div class="crumb svg crumbhome">
            <a href="https://next.cybercorp.fr/apps/gestion/" class="icon-home">Home</a>
            <span style="display: none;"></span>
        </div>
        <div class="crumb svg crumbhome">
            <span>Devis</span>
        </div>
        <div class="crumb svg crumbhome">
            <a><span id="newDevis">Ajouter un Devis</span></a>
        </div>
    </div>
    <table id="devis" class="display" style="table-layout: fixed; width: 100%; white-space: pre-wrap;">
        <thead>
            <tr>
                <th>Action(s)</th>
                <th>date</th>
                <th>Numéro</th>
                <th>Client</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>