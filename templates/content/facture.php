<div id="contentTable">
    <div class="breadcrumb" data-html2canvas-ignore>
        <div class="crumb svg crumbhome">
            <a href="https://next.cybercorp.fr/apps/gestion/" class="icon-home">Home</a>
            <span style="display: none;"></span>
        </div>
        <div class="crumb svg crumbhome">
            <span>Facture</span>
        </div>
        <div class="crumb svg crumbhome">
            <a><span id="newFacture">Ajouter un Facture</span></a>
        </div>
    </div>
    <table id="facture" class="display" style="table-layout: fixed; width: 100%; white-space: pre-wrap;">
        <thead>
            <tr>
                <th>Action(s)</th>
                <th>Numero</th>
                <th>date</th>
                <th>date_paiement</th>
                <th>type de paiement</th>
                <th>Devis</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>