<div id="contentTable">
<div class="breadcrumb" data-html2canvas-ignore>
        <div class="crumb svg crumbhome">
            <a href="https://next.cybercorp.fr/apps/gestion/" class="icon-home">Home</a>
            <span style="display: none;"></span>
        </div>
        <div class="crumb svg crumbhome">
            <span>Produit</span>
        </div>
        <div class="crumb svg crumbhome">
            <a><span id="newProduit">Ajouter un Produit</span></a>
        </div>
    </div>
	<table id="produit" class="display" style="table-layout: fixed; width: 100%; white-space: pre-wrap;">
        <thead>
            <tr>
                <th>Action(s)</th>
                <th>reference</th>
                <th>description</th>
                <th>prix unitaire</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
	</table>
</div>