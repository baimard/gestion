<ul class="app-navigation">
	<li class="app-navigation-entry"><span class="navmarg icon-add"></span><a class="a-entry" href="/apps/gestion">Client</a>
		<div class="app-navigation-entry-utils">
			<ul>
			<li class="app-navigation-entry-utils-counter"><span id="statsclient"><div class="loader"></div></span></li>
				<!-- <li class="app-navigation-entry-utils-menu-button"> -->
				<!-- <button class='menu' data-menu="client"></button> -->
				<!-- <div id="menu-client" class="app-navigation-entry-menu">
					<ul>
						<li><a id="newClient"><span class="icon-add"></span></a></li>
					</ul>
				</div> -->
				<!-- </li> -->
			</ul>
		</div>
	</li>
	<li class="app-navigation-entry"><span class="navmarg icon-template-add"></span><a href="/apps/gestion/devis">Devis</a>
		<div class="app-navigation-entry-utils">
			<ul>
			<li class="app-navigation-entry-utils-counter"><span id="statsdevis"><div class="loader"></div></span></li>
			</ul>
		</div>
	</li>
	<li class="app-navigation-entry"><span class="navmarg icon-toggle-pictures"></span><a href="/apps/gestion/facture">Facture</a>
		<div class="app-navigation-entry-utils">
			<ul>
				<li class="app-navigation-entry-utils-counter"><span id="statsfacture"><div class="loader"></div></span></li>
			</ul>
		</div>	
	</li>
	<li class="app-navigation-entry"><span class="navmarg icon-category-integration"></span><a href="/apps/gestion/produit">Produit</a>
		<div class="app-navigation-entry-utils">
			<ul>
			<li class="app-navigation-entry-utils-counter"><span id="statsproduit"><div class="loader"></div></span></li>
			</ul>
		</div>	
	</li>
</ul>