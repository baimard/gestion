<div id="app-settings">
	<div id="app-settings-header">
		<button class="settings-button" data-apps-slide-toggle="#app-settings-content">
			Configuration
		</button>
	</div>
	<div id="app-settings-content">
		<ul>
			<li><label>Dossier d'enregistrement</label><input id="theFolder" data-table="configuration" data-column="path" data-id="" type="text" placeholder="Veuillez choisir un dossier"></li>
			<li><span class="icon-rename"></span><a href="/apps/gestion/config">Mon entreprise</a></li>
		</ul>
	</div>
</div>
